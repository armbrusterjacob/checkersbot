#include <vector>
#include <cstddef>

template<typename T>
struct Node
{
    T key;
    std::vector<Node*> children;
    Node<T>* parent;

    Node(T _key)
    {
        key = _key;
    }
};

template <class T>
class MiniMax
{
    public:
    
    Node<T>* root;
    bool isMaxFirst;

    MiniMax<T>(bool _isMaxFirst)
    {
        root = nullptr;
        isMaxFirst = _isMaxFirst;
    }

    ~MiniMax<T>()
    {
        clear();
    }

    Node<T>* insert(T key, Node<T>* parent)
    {
        Node<T>* node = new Node<T>(key);
        parent->children.push_back(node);
        return node;
    }

    Node<T>* insertRoot(T key)
    {
        if(!root)
        {
            Node<T>* node = new Node<T>(key);
            root = node;
        }else{
            // probably not a good idea
            erase(root);
        }
    }

    void erase(Node<T>* node)
    {
        for(size_t i = 0; i < node->children.size(); i++)
        {
            erase(node->children.at(i));
        }

        if(node)
        {
            delete node;
        }
    }

    void clear()
    {
        if(root)
        {
            erase(root);
        }
    }

    int getDepth(Node<T>* node)
    {
        Node<T>* current = node;
        int total = -1;
        while(current)
        {
            total++;
            current = node->parent;
        }
        return total;
    }

    bool nodeIsMaximizer(Node<T>* node)
    {
        int depth = getDepth(node);
        if(isMaxFirst)
        {
            return depth % 2;
        }else
        {
            return depth + 1 % 2;
        }
    }




};