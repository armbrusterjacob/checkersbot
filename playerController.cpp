#include <cctype>
#include "controller.cpp"

enum EndState
{
    fail = -1,
    move = 0,
    hop = 1,
};

class PlayerController : public Controller
{
    const int ALPHA_ASCII_OFFSET = 65;
    const int NUM_ASCII_OFFSET = 48;

    public:
    GameMove generateMove(CheckersState& gamestate) override
    {
        return getPlayerInput(gamestate);
    }

    GameMove getPlayerInput(CheckersState& gamestate)
    {
        std::string input;
        GameMove move;
        while(true)
        {
            // First get what peice to move
            do
            {
                std::cout << "Board:" << std::endl;
                std::cout << gamestate.to_string() << std::endl;
                std::cout << "Select a peice (format like \'B2\'): ";
                std::cin >> input;
                std::cout << std::endl;
            }while(!isValidSelection(gamestate, input));

            int x = (char) input[0] - ALPHA_ASCII_OFFSET;
            int y = (char) input[1] - NUM_ASCII_OFFSET;
            move.push_back(Position(x,y));


            // Now get what peice to move to as well as possible hops
            EndState state = EndState::fail;
            while(true)
            {
                // Create a temporary clone of the gamestate to act upon
                CheckersState tempState = CheckersState(gamestate);
                bool isHop = false;
                if(move.size() >= 2)
                {
                    isHop = true;
                    tempState.applyMove(move);
                    std::cout << tempState.to_string() << std::endl;
                }

                std::cout << "d - deselect, s - stop hopping" << std::endl;
                std::cout << "(Current move: " << move.to_string() << ") Move to...: "<< std::endl;
                std::cin >> input;
                // deselect and go back to selecting
                if(input == "d")
                    break;
                // stop hopping
                if(input == "s")
                {
                    if(move.size() >= 2)
                    {
                        break;
                    }else
                    {
                        std::cout << "You aren't currently hopping." << std::endl;
                        continue;
                    }
                }

                if(isCorrectFormat(input))
                {
                    int x = (char) input[0] - ALPHA_ASCII_OFFSET;
                    int y = (char) input[1] - NUM_ASCII_OFFSET;

                    GameMove tempMove;
                    tempMove.push_back(move.back());
                    move.push_back(Position(x,y));
                    tempMove.push_back(move.back());

                    state = isValidMove(tempState, tempMove, isHop);

                    if(state == EndState::move)
                    {
                        break;
                    }else if(state == EndState::fail)
                    {
                        std::cout << "Invalid move." << std::endl << std::endl;
                        move.erase(move.end());
                    }else
                    {
                        
                        // std::cout << "hop" << std::endl;
                    }
                }else
                {
                    std::cout << "Incorrect format." << std::endl << std::endl;
                }
            }

            // Optionally start back from the beginning
            if(input == "d")
            {
                move.clear();
                continue;
            }
            else
                break;
        }

        return move;
    }

    bool isCorrectFormat(std::string posStr)
    {
        if(posStr.size() < 2)
            return false;
        if(!isalpha(posStr[0]))
            return false;
        if(!isdigit(posStr[1]))
            return false;
        
        return true;
    }

    bool isValidSelection(CheckersState& gamestate, std::string posStr)
    {
        // First check if the move is in the correct format (format is [alpha][digit])
        if(!isCorrectFormat(posStr))
            return false;
        
        // Now check if you can actually move that piece
        int x = (char) posStr[0] - ALPHA_ASCII_OFFSET;
        int y = (char) posStr[1] - NUM_ASCII_OFFSET;
        // Check to see if input is in range
        if(x >= COLS || x < 0)
            return false;
        if(y >= ROWS || y < 0)
            return false;
        if(gamestate.board[y][x].isEmpty)
            return false;
        if(gamestate.board[y][x].isRed != gamestate.board[y][x].isRed)
            return false;

        return true;
    }

    EndState isValidMove(CheckersState& gamestate, GameMove move, bool isHop = false)
    {
        // Now check if you can actually move that piece
        auto board = gamestate.board;
        Position currPos = move.front();

        if(board[currPos.y][currPos.x].isKing)
        {
            return isValidMoveKing(gamestate, move, isHop);
        }else
        {
            return isValidMoveMan(gamestate, move, isHop);
        }

    }

    EndState isValidMoveMan(CheckersState& gamestate, GameMove move, bool isHop = false)
    {
        if(move.size() < 2)
            return EndState::fail;
        
        // Now check if you can actually move that piece
        auto board = gamestate.board;
        Position currPos = move.front();
        Position nextPos = move[1];

        // Check to see if input is in range
        if(currPos.x >= COLS || currPos.x < 0 || currPos.y >= ROWS || currPos.y < 0)
            return EndState::fail;
        if(nextPos.x >= COLS || nextPos.x < 0 || nextPos.y >= ROWS || nextPos.y < 0)
            return EndState::fail;

        // Make sure the player is moving a peice and moving it to an unocuppied space
        if(board[currPos.y][currPos.x].isEmpty)
            return EndState::fail;
        if(!board[nextPos.y][nextPos.x].isEmpty)
            return EndState::fail;

        // Make sure we're moving in the correct direction the man should
        int diff = currPos.y - nextPos.y;
        if(board[currPos.y][currPos.x].isRed && diff < 0)
            return EndState::fail;
        if(!board[currPos.y][currPos.x].isRed && diff > 0)
            return EndState::fail;

        // check how far we're moving to see if we're moving or hopping
        int distX = abs(currPos.x - nextPos.x);
        int distY = abs(currPos.y - nextPos.y);
        if(!isHop && distX == 1 && distY == 1)
        {
            return EndState::move;
        }else if(distX == 2 && distY == 2)
        {
            // get x and y positions of the peice in the middle of a hop
            int x = (currPos.x + nextPos.x) / 2;
            int y = (currPos.y + nextPos.y) / 2;
            if(board[y][x].isEmpty)
                return EndState::fail;
            if(board[y][x].isRed == board[currPos.y][currPos.x].isRed)
                return EndState::fail;

            if(move.size() == 2)
                return EndState::hop;
            else
            {
                move.erase(move.begin());
                // If the peice will be kinged by the new position
                if(nextPos.y == 0 || nextPos.y == ROWS - 1)
                    return isValidMoveKing(gamestate, move, true);
                else
                    return isValidMoveMan(gamestate, move, true);
            }
        }else
        {

            // we're either not moving at all or more than possible for either x or y
            return EndState::fail;
        }
    }
    
    EndState isValidMoveKing(CheckersState& gamestate, GameMove move, bool isHop = false)
    {
        if(move.size() < 2)
            return EndState::fail;
        
        // Now check if you can actually move that piece
        auto board = gamestate.board;
        Position currPos = move.front();
        Position nextPos = move[1];
        // Check to see if input is in range

        if(currPos.x >= COLS || currPos.x < 0 || currPos.y >= ROWS || currPos.y < 0)
            return EndState::fail;
        if(nextPos.x >= COLS || nextPos.x < 0 || nextPos.y >= ROWS || nextPos.y < 0)
            return EndState::fail;

        if(board[currPos.y][currPos.x].isEmpty)
            return EndState::fail;
        if(!board[nextPos.y][nextPos.x].isEmpty)
            return EndState::fail;

        // check how far we're moving to see if we're moving or hopping
        int distX = abs(currPos.x - nextPos.x);
        int distY = abs(currPos.y - nextPos.y);
        if(!isHop && distX == 1 && distY == 1)
        {
            return EndState::move;
        }else if(distX == 2 && distY == 2)
        {
            // get x and y positions of the peice in the middle of a hop
            int x = (currPos.x + nextPos.x) / 2;
            int y = (currPos.y + nextPos.y) / 2;
            if(board[y][x].isEmpty)
                return EndState::fail;
            if(board[y][x].isRed == board[currPos.y][currPos.x].isRed)
                return EndState::fail;

                // std::cout << "x=" << x << ", y=" << y << std::endl;
            if(move.size() == 2)
                return EndState::hop;
            else
            {
                move.erase(move.begin());
                return isValidMoveKing(gamestate, move, true);
            }
        }else
        {
            // we're either not moving at all or more than possible
            return EndState::fail;
        }
    }



};