#pragma once
#include "checkersState.cpp"

class Controller
{
    public:
    virtual GameMove generateMove(CheckersState& gamestate) = 0;
    ~Controller(){};
};