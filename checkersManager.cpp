#include <iomanip>
#include <iostream>
#include <cctype>
#include "playerController.cpp"
#include "botController.cpp"

class CheckersManager
{
    Controller* redController;
    Controller* blackController;

    public:
    void playGame()
    {
        initializeGame();

        // create a board with black going first
        CheckersState game = CheckersState(Color::black);
        // initilize board to how a checkers game should start
        game.setInitialState();

        int turn = 1;
        while(!game.endState())
        {
            GameMove move;
            if(game.isRedTurn)
            {
                std::cout << "------- RED'S TURN -------" << std::endl;
                move = redController->generateMove(game);
            }
            else
            {
                std::cout << "------- BLACKS'S TURN -------" << std::endl;
                move = blackController->generateMove(game);
            }

            if(!game.applyMove(move))
            {
                std::cout << "couldn't apply game move... stoping game..." << std::endl;
                return;
            }
            std::cout << std::endl;
            turn++;
        }

    }

    void initializeGame()
    {
        std::string redInput, blackInput;

        if(redController)
            delete redController;
        if(blackController)
            delete redController;

        bool validInput = false;
        while(!validInput)
        {
            std::cout << "Red Player" << std::endl;
            std::cout << "  1. Player" << std::endl;
            std::cout << "  2. Bot" << std::endl;
            std::cin >> redInput;
            std::cout << std::endl;

            std::cout << "Black Player" << std::endl;
            std::cout << "  1. Player" << std::endl;
            std::cout << "  2. Bot" << std::endl;
            std::cin >> blackInput;
            std::cout << std::endl;

            if(redInput.size() < 1 || blackInput.size() < 1)
                continue;
            if(isdigit(redInput[0]) && isdigit(blackInput[0]))
            {
                const int NUM_ASCII_OFFSET = 48;
                int redNum = redInput[0] - NUM_ASCII_OFFSET;
                int blackNum = blackInput[0] - NUM_ASCII_OFFSET;

                if(redNum >= 1 && blackNum >= 1 && redNum < 2 && blackNum < 2)
                {
                    if(redNum == 1)
                        redController = new PlayerController();
                    else
                        redController = new BotController();

                    if(blackNum == 1)
                        blackController = new PlayerController();
                    else
                        blackController = new BotController();
                    
                    validInput = true;
                }
            }
        }

        return;
    }


    void testGameState()
    {
        // Test Gamestate funcitonality

        blackController = new PlayerController();
        // start game with red first
        CheckersState game = CheckersState(Color::black);
        // game.setInitialState();
        game.setSpace(2, 5, Color::black);
        game.setSpace(3, 6, Color::red);
        game.setSpace(5, 6, Color::red);

        blackController->generateMove(game);

        // std::cout << "Inital States:" << std::endl;
        // std::cout << game.to_string() << std::endl;

        // std::vector<CheckersState*> states;
        // game.generateValidMoves(states);
        // std::cout << "List of possible result states from this state: (count = " << states.size() << ")" << std::endl;
        // for(size_t i = 0; i < states.size(); i++)
        // {
        //      std::cout << states[i]->to_string() << std::endl;
        // }
    }
    

};