#pragma once
#include <iostream>
#include <stdlib.h>
#include <string>
#include <vector>
#include <cmath>

const int ROWS = 8;
const int COLS = 8;

enum Color
{
    black = 0,
    red = 1,
};

struct Space
{
    bool isRed = false;
    bool isEmpty = false;
    bool isKing = false;
};

struct Position
{
    int x;
    int y;

    Position(int _x, int _y) : x(_x), y(_y) {;}

    char alphaRep(int num)
    {
        const int ASCII_OFFSET = 65;
        return num + ASCII_OFFSET;
    }

    /*
    *    Note that this doesn't represent how it's stored in the 2D array
    */
    std::string to_string()
    {
        std::string str = "";
        str += alphaRep(x);
        str += std::to_string(y);
        return str;
    }

    bool inRange()
    {
        if(x < 0 || x >= COLS || y < 0 || y >= ROWS)
            return false;
        return true;
    }
};

/*
    GameMove
        Stores a sequence of positions where every pair represents a move
        ex) [0] and [1] are a pair, and  [1] and [2] are a pair.
*/
struct GameMove : std::vector<Position>
{
    std::string to_string()
    {
        std::string str = "";
        for(size_t i = 0; i < size() - 1; i++)
        {
            str += at(i).to_string() + "->";
        }
        str += at(size() - 1).to_string();
        return str;
    }

    bool inRange()
    {
        bool test = true;
        for(size_t i = 0; i < size() - 1; i++)
        {
            test = at(i).inRange();
        }
        return test;
    }
};

class CheckersState
{
    public:
    Space board[ROWS][COLS];
    bool isRedTurn = false;

    void setTurn(bool _isRedTurn) {isRedTurn = _isRedTurn;} 
    bool getTurn() {return isRedTurn;} 

    /*
    *   Constructor
    */
    CheckersState(bool isRedFirst)
    {
        isRedTurn = isRedFirst;
        clear();
        // setInitialState();
    }

    /*
    *   Copy Constructor
    */
    CheckersState(const CheckersState &_state)
    {
        to_string();
        isRedTurn = _state.isRedTurn;
        for(size_t i = 0; i < ROWS; i++)
        {
            for(size_t j = 0; j < COLS; j++)
            {
                board[i][j].isEmpty = _state.board[i][j].isEmpty;
                board[i][j].isRed = _state.board[i][j].isRed;
                board[i][j].isKing = _state.board[i][j].isKing;
            }
        }
    }


    /*
    *   setInitialState()
    *       Initialize board to a starting game of Checkers
    */
    void setSpace(int x, int y, bool color = Color::black, bool _isKing = false, bool _isEmpty = false)
    {
        board[y][x].isEmpty = _isEmpty;
        board[y][x].isRed = color;
        board[y][x].isKing = _isKing;

        return;
    }

    /*
    *   setInitialState()
    *       Initialize board to a starting game of Checkers
    */
    void setInitialState()
    {
        clear();

        const int placedRows = 3;
        for(size_t i = 0; i < placedRows; i++)
        {
            // Place black peices
            for(int j = (i + 1) % 2; j < COLS; j +=2) 
            {
                board[i][j].isRed = false;
                board[i][j].isEmpty = false;
            }

            // Place red peices
            int inversePos = COLS - 1 - i;
            for(int j = (inversePos + 1) % 2; j < COLS; j +=2) 
            {
                board[inversePos][j].isRed = true;
                board[inversePos][j].isEmpty = false;
            }
        }
    }

    /*
    *   clear()
    *       Clear entire board to be default empty values
    */
    void clear()
    {
        for(size_t i = 0; i < ROWS; i++)
        {
            for(size_t j = 0; j < COLS; j++)
            {
                board[i][j].isEmpty = true;
                board[i][j].isRed = true;
                board[i][j].isKing = false;
            }
        }
    }

    /*
    *   endState()
    *       Check if the game has ended and a player has won
    */
    bool endState()
    {
        int redCount;
        int blackCount;

        for(size_t i = 0; i < ROWS; i++)
        {
            for(size_t j = 0; j < COLS; j++)
            {
                if(!board[i][j].isEmpty)   
                {
                    board[i][j].isRed ? redCount++ : blackCount++;
                }
            }
        }

        if(blackCount == 0 || redCount == 0)
        {
            return true;
        }

        return false;
    }


    /*
    *   eval()
    *       Evaluate the board state to a score for use in a minimax tree
    */
    float eval()
    {
        // currently a simple evalutaion that counts peices
        int redCount = 0;
        int blackKingCount = 0;
        int blackCount = 0;
        int redKingCount = 0;

        for(size_t i = 0; i < ROWS; i++)
        {
            for(size_t j = 0; j < COLS; j++)
            {
                if(!board[i][j].isEmpty)   
                {
                    if(board[i][j].isRed)
                    {
                        board[i][j].isKing ? redCount++ : redKingCount++;
                    }else
                    {
                        board[i][j].isKing ? blackCount++ : blackKingCount++;
                    }
                    
                }
            }
        }

        // Black is maximizer, Red is minimizer
        const float KING_MULTIPLER = 1;
        float score = blackCount + (blackCount * KING_MULTIPLER);
        score -= redCount - (redKingCount * KING_MULTIPLER);
        return score;
    }

    /*
    *   applyMove(Gamemove move)
    *       Return true if the move is valid, false if invalid.      
    *       (Currently does not check if peices are actually placed diagonally
    *       like a normal checkers game might and obviously the code breaks if
    *       if their not placed right. However it might not need to check this
    *       if its checked for validity elsewhere)
    *       Todo: hops, check diagonals
    */
    bool applyMove(GameMove moves)
    {
        // If all moves are in range and we're actually moving a peice
        if(!moves.inRange() || board[moves[0].y][moves[0].x].isEmpty)
            return false;
        // For every pair of positions
        // std::cout << "got here" << std::endl;
        for(size_t i = 0; i < moves.size() - 1; i++)
        {
            Position currPos = moves[i];
            Position nextPos = moves[i+1];
            // If there currently is a peice where we're moving to it's invalid
            if(!board[nextPos.y][nextPos.x].isEmpty)
                return false;
            // If the move is a hop and there isn't a peice in between it's invalid
            if(abs(currPos.x - nextPos.x) == 2)
            {
                // get x and y positions of the peice in  the middle of a hop
                int x = (currPos.x + nextPos.x) / 2;
                int y = (currPos.y + nextPos.y) / 2;
                if(board[y][x].isEmpty)
                    return false;
                // If you're trying to hop over a peice that's the same color it's invalid
                if(board[currPos.y][currPos.x].isRed == board[y][x].isRed)
                    return false;

                // now we know we're hopping over an enemy peice so we can remove that enemy peice
                board[y][x].isEmpty = true;
            }
            
            // Now apply a move
            board[currPos.y][currPos.x].isEmpty = true;
            board[nextPos.y][nextPos.x].isEmpty = false;
            board[nextPos.y][nextPos.x].isRed = board[currPos.y][currPos.x].isRed;
            board[nextPos.y][nextPos.x].isKing = board[currPos.y][currPos.x].isKing;

            // Check to see if we have to king a peice
            if(nextPos.y == 0 || nextPos.y == ROWS - 1)
            {
                board[nextPos.y][nextPos.x].isKing = true;
            }
        }
        isRedTurn ? isRedTurn = false : isRedTurn = true;
        return true;
    }

    std::string to_string(bool alphaRow = true)
    {
        const int INT_TO_ASCII_OFFSET = 65;
        // Print top letters
        
        std::string str = "   ";
        for(size_t i = 0; i < COLS; i++)
        {
            if(alphaRow)
            {
                str += (char) (i + INT_TO_ASCII_OFFSET);
                str += " ";
            }
            else
                str +=  std::to_string(i) + " ";
        }
        str += "\n";
        
        str += "  +" + std::string(COLS * 2, '-') + "+\n";
        for(size_t i = 0; i < ROWS; i++)
        {
            str += " " + std::to_string(i) + "|";
            for(size_t j = 0; j < ROWS; j++)
            {
                if(board[i][j].isEmpty)
                {
                    if((i + j) % 2 == 0)
                        str += "░░";
                    else
                        str += "▒▒";
                }else
                {
                    if(board[i][j].isRed)
                        str += "R";
                    else
                        str += "B";

                    if(board[i][j].isKing)
                        str += "K";
                    else
                        str += "M";
                }
            }
            str += "|\n";
        }
        str += "  +" + std::string(COLS * 2, '-') + "+\n";
        return str;
    }

    CheckersState* createStateFromMove(const std::vector<CheckersState*> states, int x, int y, GameMove moves)
    {
        CheckersState* state = new CheckersState(*this);
        GameMove newMoves = GameMove(moves);
        newMoves.push_back(Position(x, y));
        if(!state->applyMove(newMoves))
        {
            std::cout << to_string() << std::endl;
            std::cerr << "couldn't apply move, " << newMoves.to_string()  << std::endl;
            exit(1);
        }
        return state;
    }

    /*
    *    getMovesFromMan(std::vector<CheckersState>* states, int x, int y)
    *        Helper function for generateValidMoves() that adds gamestates to the vector
    *        for whichever current man it has been assigned.
    *        This version is specific to men who can only move forward, where "forward"
    *        depends on which color you are.
    */
    void getMovesFromMan(std::vector<CheckersState*>& states, int x, int y, GameMove moves, bool isHop = false)
    {
        int nextY, hopY;
        if(isRedTurn)
        {
            nextY = y - 1;
            hopY = y - 2;
        }else
        {
            nextY = y + 1;
            hopY = y + 2;
        }

        // for the left and right sides
        for(int i = -1; i < 2; i += 2)
        {
            int nextX = x + i;
            // Check the immediate spots you can move to
            if(!isHop)
            {
                if(nextX >= 0 && nextX < COLS && board[nextY][nextX].isEmpty)
                {
                    states.push_back(createStateFromMove(states, nextX, nextY, moves));
                }
            }

            // Check hop bounds to see if a hop is even possible
            int hopX = nextX + i;
            if(hopY < 0 || hopY >= ROWS || hopX < 0 || hopX >= COLS)
                continue;

            // Check if hop spot is available
            if(board[hopY][hopX].isEmpty)
            {
                // here nextY and next X are in the same position peice between the hop
                // check to see if they exist and are of the oppsoite team
                if(!board[nextY][nextX].isEmpty && board[nextY][nextX].isRed != isRedTurn)
                {
                    states.push_back(createStateFromMove(states, hopX, hopY, moves));
                    GameMove newMoves;
                    newMoves.push_back(Position(hopX, hopY));

                    auto lastState = states.at(states.size() - 1);
                    // Check to see if the peice can now be kinged
                    if(hopY == 0 || hopY == ROWS - 1){
                        // Recursively call this funciton again for hop sequences
                        lastState->getMovesFromKing(states, hopX, hopY, newMoves, true);
                    }else
                    {
                        // Recursively call this funciton again for hop sequences
                        lastState->getMovesFromMan(states, hopX, hopY, newMoves, true);
                    }
                }
            }
        }

        return;
    }

    /*
    *    getMovesFromKing(std::vector<CheckersState>* states, int x, int y)
    *        Helper function for generateValidMoves() that adds gamestates to the vector
    *        for whichever current king it has been assigned.
    *        This version is specific to kings who can move in any direction regardless of
    *        color.
    */
    void getMovesFromKing(std::vector<CheckersState*>& states, int x, int y, GameMove moves, bool isHop = false)
    {
        // for the left and right sides
        for(int i = -1; i < 2; i += 2)
        {
            int nextX = x + i;
            int hopX = nextX + i;
            // for the top and bottom sides
            for(int j = -1; j < 2; j += 2)
            {    
                int nextY = y + j;
                int hopY = nextY + j;
                // Check the immediate spots you can move to
                if(!isHop)
                {
                    // Check to see if the next values are in range
                    if(nextY < 0 || nextY >= ROWS || nextX < 0 || nextX >= COLS)
                        continue;

                    if(board[nextY][nextX].isEmpty)
                    {
                        states.push_back(createStateFromMove(states, nextX, nextY, moves));
                    }
                }

                // Check hop bounds to see if a hop is even possible
                if(hopY < 0 || hopY >= ROWS || hopX < 0 || hopX >= COLS)
                    continue;

                // Check if hop spot is available
                if(board[hopY][hopX].isEmpty)
                {
                    // here nextY and next X are in the same position peice between the hop
                    // check to see if they exist and are of the oppsoite team
                    if(!board[nextY][nextX].isEmpty && board[nextY][nextX].isRed != isRedTurn)
                    {
                        states.push_back(createStateFromMove(states, hopX, hopY, moves));
                        GameMove newMoves;
                        newMoves.push_back(Position(hopX, hopY));
                        auto lastState = states.at(states.size() - 1);
                        lastState->getMovesFromKing(states, hopX, hopY, newMoves, true);
                    }
                }
            }
        }
    }

    /*
    *   generateValidMoves()
    *       Generate a vector consisting of every possible board state that a player can
    *       reach using their currrent peices
    */
    std::vector<CheckersState*> generateValidMoves(std::vector<CheckersState*>& states)
    {
        // for every peice that matches the current turn's color
        for(size_t i = 0; i < COLS; i++)
        {
            for(size_t j = 0; j < ROWS; j++)
            {
                if(board[i][j].isEmpty || board[i][j].isRed != isRedTurn)
                    continue;
                GameMove moves = GameMove();
                moves.push_back(Position(j, i));
                if(board[i][j].isKing)
                    getMovesFromKing(states, j, i, moves);
                else
                    getMovesFromMan(states, j, i, moves);
            }   
        }
        
        return states;
    }

};